/******************************************************************************
 *
 * LPC2119 Demo - stdio with newlib and analog to digital conversion
 * by Martin THOMAS <eversmith@heizung-thomas.de>
 *
 * Reads voltage at PINA0 in "Singe-Conversion/Burst-Mode"
 * and uses stdio/printf to view the result.
 *
 * ----------------------------------------------------------------------------
 *
 * - UART functions based on code from Bill Knight (R O Software)
 * - newlib stdio-interface from newlib-lpc (Aeolus Development)
 *   (may be easier to port to other targets in this form)
 * - V/V3A->V conversion based on an example found somewhere in the net
 *
 * ----------------------------------------------------------------------------
 *
 *  Tested with WinARM 4/05 (arm-elf-gcc 4.0.0) and 
 *  a Philips LPC2129 on a LPC-P2129 board (Olimex)
 *
 *  Review 20060710: 
 *  - updated makefile
 *  - test with WinARM 6/06 (arm-elf-gcc 4.1.1)
 *  Reviews 200603**
 *  - added fixes/improvements provided by Alexey Shusharin 
 *
 *****************************************************************************/

/*
	With arm-elf-gcc 4.0.0, newlib 1.13, Thumb-mode:
	section            size         addr
	.text             12939            0
	.data              2088   1073741824
	.bss                 68   1073743912
	.stack             1024   1073744128
	
	ARM-Mode:
	section            size         addr
	.text             18011            0
	
	With arm-elf-gcc 4.1.1, newlib 1.14, updated makefile
	             section            size         addr
	Thumb-mode:  .text             11208            0
	ARM-mode:    .text             15692            0
*/

#include "algoritmo.h"
#include <stdio.h>

#include "config.h" /* Clock config */
#include "uart.h"
#include "armVIC.h"

#define BAUD 38400

/* Voltage at V_ddA/V3A (LPC2129 Pin7) */
/* connected to 3,3V on LPC-P2129      */
#define VREF  33/10  


/* 
 *  Inicializa os periféricos principais:
 *    PLL - Multiplica a frequencia do cristal para conseguir rodar a 60MHz
 *    MAM - Módulo acelerador de memória - Aumenta o desempenho fazendo pré-fetch da memória.
 */
static void systemInit(void)
{
	// --- enable and connect the PLL (Phase Locked Loop) ---
	// a. set multiplier and divider
	PLLCFG = MSEL | (0<<PSEL1) | (1<<PSEL0);
	// b. enable PLL
	PLLCON = (1<<PLLE);
	// c. feed sequence
	PLLFEED = PLL_FEED1;
	PLLFEED = PLL_FEED2;
	// d. wait for PLL lock (PLOCK bit is set if locked)
	while (!(PLLSTAT & (1<<PLOCK)));
	// e. connect (and enable) PLL
	PLLCON = (1<<PLLE) | (1<<PLLC);
	// f. feed sequence
	PLLFEED = PLL_FEED1;
	PLLFEED = PLL_FEED2;
	
	// --- setup and enable the MAM (Memory Accelerator Module) ---
	// a. start change by turning off the MAM (redundant)
	MAMCR = 0;	
	// b. set MAM-Fetch cycle to 3 cclk as recommended for >40MHz
	MAMTIM = MAM_FETCH;
	// c. enable MAM 
	MAMCR = MAM_MODE;
	
	// --- set VPB speed ---
	VPBDIV = VPBDIV_VAL;
	
        
	// --- map INT-vector ---
	#if defined(RAM_RUN)
	  MEMMAP = MEMMAP_USER_RAM_MODE;
	#elif defined(ROM_RUN)
	  MEMMAP = MEMMAP_USER_FLASH_MODE;
	#else
	#error RUN_MODE not defined!
	#endif
}


int tempo, tempoAnterior, dif;
static void IntHandler(void)  __attribute__ ((interrupt("IRQ")));
static void IntHandler(void)
{
	tempoAnterior = tempo;
	tempo = T1CR2;
	dif = tempo - tempoAnterior;
	printf("!\n");
	VICVectAddr = 0x00;    // dummy write to VIC to signal end of ISR 	
}

/*
 * Método principal. Inicializa os periféricos principais, a porta serial e os pinos de entrada e saída.
 * Depois de configurar tudo, chama "funciona()" no arquivo algoritmo.c, que contém a lógica de funcionamento.
 */
int main(void) 
{
        //13->1V
        //12-1V
        
        //Lista de pinos que devem ser configurados como "OUT". (Padrão é "IN")
        char pinos_out[]= {8,9,6,7,10,16,12,13};
        int dirMask = 0; //Variavél temporária com a mascara de Entrada/Saida.
        int i, j; //Contador Genérico
        int clear, set;
        
	for (i=0; i<50000; i++) {}
        
         /* inicia PLL, MAM etc. */       
	systemInit();
             
        /* inicia a Serial */
        uart0Init(UART_BAUD(BAUD), UART_8N1, UART_FIFO_8);        
        
        /*Configura os pinos de saida*/
        for (i=0; i<sizeof(pinos_out); i++) {
                dirMask |= 1<<pinos_out[i];
        }
        IODIR0 = dirMask;
        
        
        /*COnfigura o DAC*/
        PINSEL1 |= 1<<19;
        PINSEL1 &= ~1<<18;

	//Coloquei o Controle Futaba no pino 47 = P0.17 = Cap1.2
	/*Configura o Cap1.2*/
	PINSEL1 |= 1<<2;
	PINSEL1 &= ~1<<3;


	//---Jeito  Côxo, serve pra ver se o controle tá funcionando.
	int num1 = 0;
	while (1) {
		if (IOPIN0&1<<17) {
			uart0Putch('1');
			num1++;
		 } else {
			if (num1 > 10) {
				uart0Putch(10);
				uart0Putch(13);
			}
			num1=0;
			uart0Putch('_');
		}
	}

       
        //T1CTCR = 10; //Conta as bordas do clock
	T0IR=0xFF; 
	T0MR0 = 12000000/2; //Periodo de 500ms
	T0MCR = 3; //Reset & Interrupt on match
	T0TCR = 2; //Reseta Timer
	T0TCR = 1; //Liga o Timer

	//Iterrompe na borda de subida da Cap1.2
	//T1CCR = 1<<6 | 1<<7; 

	VICIntSelect = 0x00000000; // All Interrrupts are IRQ
	//VICVectAddr = 0; // Clear interrupt
	VICIntEnClr = 0xFFFFFFFF; // Diasable all interrupts

	//Fazendo por interrupção
	(*(&VICVectCntl0)+1) = 0x20 | 0xF; // choose highest priority ISR slot 	
	(*(&VICVectAddr0)) = (int)IntHandler;
 
	// set up USB interrupt
	VICIntSelect &= ~(1<<0xF);               // select IRQ for Timer1
	VICIntEnable |= (1<<0xF);

	enableIRQ();

	while (1) {
//		printf("Interrupts: %2x\n", T1IR);
//		printf("T=%i\n", T1TC, dif);
	}
	
	return 0; /* never reached */
}
