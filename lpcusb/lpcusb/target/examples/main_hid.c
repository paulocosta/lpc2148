/*
	LPCUSB, an USB device driver for LPC microcontrollers	
	Copyright (C) 2006 Bertrik Sikken (bertrik@sikken.nl)

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. The name of the author may not be used to endorse or promote products
	   derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
	IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
	THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
	THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "type.h"
#include "debug.h"

#include "hal.h"
#include "console.h"
#include "usbapi.h"
#include "lpc214x.h"
#include "armVIC.h"

#define INTR_IN_EP		0x81

#define MAX_PACKET_SIZE	64

#define LE_WORD(x)		((x)&0xFF),((x)>>8)

#define REPORT_SIZE			4

#define INT_VECT_NUM 0

static U8	abClassReqData[4];
static U8	abReport[] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
static int	_iIdleRate = 0;
static int	_iFrame = 0;
static char	needsRefresh = 0;

static int timerIntervals[10];
static int timerMins[10];
static int timerMaxs[10];
static int timerCurrentAxis=0;
static int axisValues[10];

// see the joystick example from the usb.org HID Descriptor Tool
static U8 abReportDesc[] = {
    0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
    0x09, 0x04,                    // USAGE (Joystick)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x09, 0x01,                    //   USAGE (Pointer)
    0xa1, 0x00,                    //   COLLECTION (Physical)
    0x09, 0x30,                    //     USAGE (X)
    0x09, 0x31,                    //     USAGE (Y)
    0x95, 0x02,                    //     REPORT_COUNT (2)
    0x81, 0x02,                    //     INPUT (Data,Var,Abs)
    0xc0,                          //   END_COLLECTION
    0x09, 0x32,                    //   USAGE (Z)
    0x0b, 0xba, 0x00, 0x02, 0x00,  //   USAGE (Simulation Controls:Rudder)
    0x95, 0x02,                    //   REPORT_COUNT (2)
    0x81, 0x02,                    //   INPUT (Data,Var,Abs)
    0xc0                           // END_COLLECTION
};








static const U8 abDescriptors[] = {

/* Device descriptor */
	0x12,              		
	DESC_DEVICE,       		
	LE_WORD(0x0110),		// bcdUSB	
	0x00,              		// bDeviceClass
	0x00,              		// bDeviceSubClass
	0x00,              		// bDeviceProtocol
	MAX_PACKET_SIZE0,  		// bMaxPacketSize
	LE_WORD(0xFFFF),		// idVendor
	LE_WORD(0x0056),		// idProduct
	LE_WORD(0x0100),		// bcdDevice
	0x01,              		// iManufacturer
	0x02,              		// iProduct
	0x03,              		// iSerialNumber
	0x01,              		// bNumConfigurations

// configuration
	0x09,
	DESC_CONFIGURATION,
	LE_WORD(0x22),  		// wTotalLength
	0x01,  					// bNumInterfaces
	0x01,  					// bConfigurationValue
	0x00,  					// iConfiguration
	0x80,  					// bmAttributes
	0x32,  					// bMaxPower

// interface
	0x09,   				
	DESC_INTERFACE, 
	0x00,  		 			// bInterfaceNumber
	0x00,   				// bAlternateSetting
	0x01,   				// bNumEndPoints
	0x03,   				// bInterfaceClass = HID
	0x00,   				// bInterfaceSubClass
	0x00,   				// bInterfaceProtocol
	0x00,   				// iInterface

// HID descriptor
	0x09, 
	DESC_HID_HID, 			// bDescriptorType = HID
	LE_WORD(0x0110),		// bcdHID
	0x00,   				// bCountryCode
	0x01,   				// bNumDescriptors = report
	DESC_HID_REPORT,   		// bDescriptorType
	LE_WORD(sizeof(abReportDesc)),

// EP descriptor
	0x07,   		
	DESC_ENDPOINT,   		
	INTR_IN_EP,				// bEndpointAddress
	0x03,   				// bmAttributes = INT
	LE_WORD(MAX_PACKET_SIZE),// wMaxPacketSize
	10,						// bInterval   		

// string descriptors
	0x04,
	DESC_STRING,
	LE_WORD(0x0409),

	// manufacturer string
	0x14,
	DESC_STRING,
	'P', 0, 'A', 0, 'U', 0, 'L', 0, 'O', 0, 'S', 0, 'o', 0, 'f', 0, 't', 0,

	// product string
	0x14,
	DESC_STRING,
	'J', 0, 'o', 0, 'y', 0, 'F', 0, 'u', 0, 't', 0, 'a', 0, 'b', 0, 'a', 0,

	// serial number string
	0x12,
	DESC_STRING,
	'D', 0, 'E', 0, 'A', 0, 'D', 0, 'C', 0, '0', 0, 'D', 0, 'E', 0,
	
	// terminator
	0
};



static void USBIntHandler(void) __attribute__ ((interrupt("IRQ")));
static void USBIntHandler(void)
{
	USBHwISR();
	VICVectAddr = 0x00;    // dummy write to VIC to signal end of ISR 	
}

static void TimerIntHandler(void) __attribute__ ((interrupt("IRQ")));
#define MILLI 60000
#define CheckRange(x) ((x > MILLI/2) && (x < 3*MILLI))
static void TimerIntHandler(void)
{
	static int TC_prev;
	timerIntervals[timerCurrentAxis] = T1CR2 - TC_prev;
	TC_prev = T1CR2;
 
	if (!CheckRange(timerIntervals[timerCurrentAxis])) {
		putchar('0'+timerCurrentAxis);
		if (timerCurrentAxis==5) { //Leu os 4 eixos -> Medida Válida!
			int i;
			for (i=0; i<4; i++) {
				//Ajusta os Máximos/Mínimos 
				if ((timerIntervals[i] < timerMins[i]) | (!CheckRange(timerMins[i])))
					timerMins[i] = timerIntervals[i];
				if ((timerIntervals[i] > timerMaxs[i]) | (!CheckRange(timerMaxs[i])))
					timerMaxs[i] = timerIntervals[i];


				//Converte os valores para que sejam usados na saída
				int newVal = (1023 * ( timerIntervals[i] - timerMins[i] )) / ( timerMaxs[i] - timerMins[i] );
				//Coloca um pouquinho de histerese
				int dif = axisValues[i]-newVal;
				dif = dif>0?dif:-dif;
				if (dif > 2) {
					axisValues[i] = newVal;
					needsRefresh = 1;
				}
			}
		}

		timerCurrentAxis=0;
	} else {
		timerCurrentAxis++;
	}

	T1IR = 0xFF;
	VICVectAddr = 0x00;    // dummy write to VIC to signal end of ISR 	
}



/*************************************************************************
	HandleClassRequest
	==================
		HID class request handler
		
**************************************************************************/
static BOOL HandleClassRequest(TSetupPacket *pSetup, int *piLen, U8 **ppbData)
{
	U8	*pbData = *ppbData;

	switch (pSetup->bRequest) {
	
   	// get_idle
	case HID_GET_IDLE:
		DBG("GET IDLE, val=%X, idx=%X\n", pSetup->wValue, pSetup->wIndex);
		pbData[0] = (_iIdleRate / 4) & 0xFF;
		*piLen = 1;
		break;

	// set_idle:
	case HID_SET_IDLE:
		DBG("SET IDLE, val=%X, idx=%X\n", pSetup->wValue, pSetup->wIndex);
		_iIdleRate = ((pSetup->wValue >> 8) & 0xFF) * 4;
		break;

	default:
		DBG("Unhandled class %X\n", pSetup->bRequest);
		return FALSE;
	}
	return TRUE;
}


#define BAUD_RATE	38400


/*************************************************************************
	HIDHandleStdReq
	===============
		Standard request handler for HID devices.
		
	This function tries to service any HID specific requests.
		
**************************************************************************/
static BOOL HIDHandleStdReq(TSetupPacket *pSetup, int *piLen, U8 **ppbData)
{
	U8	bType, bIndex;

	if ((pSetup->bmRequestType == 0x81) &&			// standard IN request for interface
		(pSetup->bRequest == REQ_GET_DESCRIPTOR)) {	// get descriptor
		
		bType = GET_DESC_TYPE(pSetup->wValue);
		bIndex = GET_DESC_INDEX(pSetup->wValue);
		switch (bType) {

		case DESC_HID_REPORT:
			DBG("DESC_HID_REPORT\n");
			// report
			*ppbData = abReportDesc;
			*piLen = sizeof(abReportDesc);
			break;

		case DESC_HID_HID:
		case DESC_HID_PHYSICAL:
		default:
			DBG("DESC_HID_HID\n");
			// search descriptor space
			return USBGetDescriptor(pSetup->wValue, pSetup->wIndex, piLen, ppbData);
		}
		
		return TRUE;
	}
	return FALSE;
}


static void HandleFrame(U16 wFrame)
{
	_iFrame++;
	if ((_iFrame > 500) || needsRefresh) {
		needsRefresh = 0;
		_iFrame = 0;

		// send report (dummy data)
		abReport[0] =  ( (axisValues[0]/4) & 0xFF); //X
		abReport[1] = ~( (axisValues[1]/4) & 0xFF); //Y
		abReport[2] =  ( (axisValues[2]/4) & 0xFF); //Throttle
		abReport[3] = ~( (axisValues[3]/4) & 0xFF); //Rudder
		USBHwEPWrite(INTR_IN_EP, abReport, REPORT_SIZE);
	}
}


/*************************************************************************
	main
	====
**************************************************************************/
void initUSB() {
	DBG("Initialising USB stack\n");

	// initialise stack
	USBInit();

	// register device descriptors
	USBRegisterDescriptors(abDescriptors);

	// register HID standard request handler
	USBRegisterCustomReqHandler(HIDHandleStdReq);

	// register class request handler
	USBRegisterRequestHandler(REQTYPE_TYPE_CLASS, HandleClassRequest, abClassReqData);

	// register endpoint
	USBHwRegisterEPIntHandler(INTR_IN_EP, NULL);

	// register frame handler
	USBHwRegisterFrameHandler(HandleFrame);

	DBG("Starting USB communication\n");

	//Fazendo USB por interrupção
	(*(&VICVectCntl0+0)) = 0x20 | 22; // choose highest priority ISR slot 	
	(*(&VICVectAddr0+0)) = (int)USBIntHandler;
	VICIntSelect &= ~(1<<22);               // select IRQ for USB
	VICIntEnable |= (1<<22);
}

void initTimer1() {
	//Coloquei o Controle Futaba no pino 47 = P0.17 = Cap1.2
	/*Configura o Cap1.2*/
	PINSEL1 |= 1<<2;
	PINSEL1 &= ~(1<<3);

	//Fazendo Timer 1 por interrupção
	(*(&VICVectCntl0+1)) = 0x20 | 5; // choose highest priority ISR slot 	
	(*(&VICVectAddr0+1)) = (int)TimerIntHandler;
	VICIntSelect &= ~(1<<5);               // select IRQ for Timer
	VICIntEnable |=  (1<<5);

	T1IR  = 0xFF; 
	T1TCR = 2; //Reseta Timer
	T1TCR = 1; //Liga o Timer
	T1CCR = (1<<6) | (1<<8);
}

void DBG_int(int v) {
	static char tab[] = "0123456789abcdef";
	int numDigits=0;
	char digits[10];
	do {
		digits[numDigits++]=v%10;
		v/=10;
	} while (v);

	do {
		putchar(tab[digits[--numDigits]]);
	} while (numDigits);
}

int main(void)
{
	int i;
	// PLL and MAM
	HalSysInit();

	// init DBG
	ConsoleInit(60000000 / (16 * BAUD_RATE));

	initUSB();
	initTimer1();
	enableIRQ();
	USBHwConnect(TRUE);

	while (1) {
		int i;
		for (i=0; i<4; i++) {
			DBG_int(axisValues[i]/4);
			DBG(" ");
		}
		DBG("\n");
		
	}

	return 0;
}

