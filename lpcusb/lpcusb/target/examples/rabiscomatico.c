/******************************************************************************
 *
 * Paulo Costa
 *
 * Arquivo principal contendo a lógica de controle do plotter.
 * Essa parte do programa se preocupa minimamente com o dispositivo utilizado, 
 * se concentrando na lógica de funcionamento.
 *
 * Com exceção deste arquivo, todo o resto do programa foi quase totalmente copiado 
 * dos projetos de Martin Thomas, em especial, usamos como base 
 * "Newlib/StdIO-Interface and LPC ADC example", disponível em:
 * http://gandalf.arubi.uni-kl.de/avr_projects/arm_projects/#gcc_stdio
 * 
 *****************************************************************************/

#define max(a,b) ((a>b)?(a):(b))
#define min(a,b) ((a<b)?(a):(b))

#include <stdio.h>
#include <string.h>
#include "rabiscomatico.h"
#include "debug.h"

//Posição Atual da caneta no papel
int curX = 0;
int curY = 0;
//Posição Atual dos passos dos motores
int cur_passoX = 0;
int cur_passoY = 0;
//Tamanho da área útil do papel. Evita que os motores tentem ultrapassar os limites e comecem a fazer barulhos feitos.
int maxX = 2750;
int maxY = 930;
//Indica se a caneta está encostada no papel
char canetaDown = 0;
//Constante de tempo usada nos "Delays"
int delayconst = 30000;

//////////// PINOS //////////////

//Pinos dos sensores de fim-de-trilho nos eixos X e Y
char sensores[]      = {30, 20};
//Pinos das luzes VERDE e VERMELHA
char luzes[]         = {18, 19};
//Pinos do motor CC que sobe/desce a caneta. 
//Ligar apenas o primeiro pino faz com que ele desça, ligar apenas o segunda faz com que ele suba.
char pinos_motorCC[] = { 4, 5}; //[desce, sobe]
//Pinos responsáveis por acionar as fases do motor X
char pinos_motorX [] = { 9, 8, 7, 6};
//Pinos responsáveis por acionar as fases do motor Y
char pinos_motorY [] = {10,16,12,13};

//Tabela de comandos para "meio-passo"
char passos[] = { 0x1,    //---X
                 0x3,    //--XX
                 0x2,    //--X-
                 0x6,    //-XX-
                 0x4,    //-X--
                 0xC,    //XX--
                 0x8,    //X---
                 0x9     //X--X
                };
#define numPassos sizeof(passos)

#define sinal(a) (a>0?1:-1)
#define abs(a)   (a>0?a:-a)

//Pára a execução por N unidade de tempo.
//A unidade de tempo é definida por "delayconst".
static void delay(int n) {
        int i,j;
        for (i=0; i<n; i++)
                for (j=0; j<delayconst; j++);
}

//Desliga os motores de passo ao fim de um movimento.
//Isso serve para poupar um pouco de potência... Mas seria dispensável.
static void paraMotores(void) {
        int clearFlags = 0;
        int i;
        for (i=0; i<4; i++) {
                clearFlags |= 1<<pinos_motorX[i];
                clearFlags |= 1<<pinos_motorY[i];
        }
        
        delay(2);
        IOCLR0 |= clearFlags;
}

//Liga/desliga as fases dos motores de passo de acordo com o passo desejado.
static void atualizaMotores(void) {
        //Listas de pinos que precisam ser ligados/desligados
        int setFlags = 0;
        int clearFlags = 0;
        int i;

        //Para cada um dos 4 pinos de cada motor
        for (i=0; i<4; i++) {
                //Verificamos a tabela de passos para o passo atual do motor X, e ligamos ou desligamos o 
                //i-ézimo pino deste de acordo com o valor lido na tabela.
                if (passos[cur_passoX] & (1<<i)) {
                        setFlags   |= 1<<pinos_motorX[i];
                } else {
                        clearFlags |= 1<<pinos_motorX[i];
                }
                
                //Mesma coisa para o motor no eixo Y
                if (passos[cur_passoY] & (1<<i)) {
                        setFlags   |= 1<<pinos_motorY[i];
                } else {
                        clearFlags |= 1<<pinos_motorY[i];
                }
        }
        
        //Liga/desliga os pinos determinados
        IOCLR0 |= clearFlags;
        IOSET0 |= setFlags;
        //Espera um pouquinho... Sem um intervalo após os movimentos, teríamos perda de passo.
        delay(1);
}



/*
 * Método auxiliar, implementa uma versão improvisada do algoritmo de Bresenham para desenho de linhas.
 * O algoritmo consiste em percorrer o eixo com maior deslocamento. A cada iteração, determina-se se o
 * outro eixo fica mais próximo da "linha ideal" na posição atual ou ao avançar em uma posição.
 *
 * Aqui, supomos que o eixo X sempre tem o maior deslocamento. Se não for o caso, a primeira coisa que 
 * o procedimento faz é trocar os eixos X e Y, para que isso seja verdade. Devido à essa gambiarra, lidamos 
 * sempre com ponteiros, ao invés de lidar com as variáveis globais
 */
static void move_aux (int* x, int* passoX, int* y, int* passoY, int deslocX, int deslocY) {
        if (abs(deslocY) > abs(deslocX)) {
                move_aux(y, passoY, x, passoX, deslocY, deslocX);
                return;

        }
        //Por praticidade, vamos trabalhar com os módulos de dX e dY. O sentido é indicado por dirX e dirY
        int dirX = sinal(deslocX);
        int dirY = sinal(deslocY);
        
        //Quanto o já moveu
        int deslocAtualX=0;
        int deslocAtualY=0;
        
        //Quanto precisa mover
        deslocX = abs(deslocX);
        deslocY = abs(deslocY);
        
        //Anda de 1 em 1 no eixo X
        for (deslocAtualX=0; deslocAtualX<deslocX;) {
                //Moveu um no eixo X
                deslocAtualX++; 
                //Manda o incremento/Decremento para a posição absoluta
                *x += dirX; 
                //Incrementa/Decrementa o passo para que o motor reflita o deslocamento
                *passoX = (*passoX + numPassos + dirX) % numPassos; 
                
        
                //Produto vetorial pode ser usado como uma medida indireta de angulo.
                //Então, fazemos 2 produtos vetoriais: Mantendo o Y no valor atual, e mudando-o.
                int crossProductSemDesloc = deslocAtualX*deslocY - (deslocAtualY  )*deslocX;
                int crossProductComDesloc = deslocAtualX*deslocY - (deslocAtualY+1)*deslocX;
                
                //E se o angulo ao Yeslocar X for menor, fazemos o deslocamento
                if (abs(crossProductComDesloc) < abs(crossProductSemDesloc)) {
                        //Moveu um no eixo Y
                        deslocAtualY++;
                        //Manda o incremento/Decremento para a posição absoluta
                        *y += dirY;
                        //Incrementa/Decrementa o passo para que o motor reflita o deslocamento
                        *passoY = (*passoY + numPassos + dirY) % numPassos;
                }
                //Atualiza os motores reais para que reflitam o deslocamento calculado.
                atualizaMotores();
        }
        //Acabou de mover, pára os motores.
        paraMotores();
}

//Move os motores para a posição X,Y especificada
static void move(int destX, int destY) {
        int deslocX = destX-curX;
        int deslocY = destY-curY;                

        //Terceiriza para o algoritmo de linha
        move_aux(&curX, &cur_passoX, &curY, &cur_passoY, deslocX, deslocY);
}






//Liga o motor CC para que a caneta suba, durate "sleep" unidades de tempo
static void sobe(int sleep) {
        if (canetaDown) {
                canetaDown = 0;
                //Começa a subir
                IOSET0 |= 1<<pinos_motorCC[1];
                //espera um pouquinho
                delay(sleep);
                //Pára
                IOCLR0 |= 1<<pinos_motorCC[1];
        }
}

//Liga o motor CC para que a caneta desça, durate "sleep" unidades de tempo
static void desce(int sleep) {
        if (!canetaDown) {
                canetaDown = 1;
                //Começa a descer
                IOSET0 |= 1<<pinos_motorCC[0];
                //espera um pouquinho
                delay(sleep);
                //Pára
                IOCLR0 |= 1<<pinos_motorCC[0];
        }        
}

//Encosta a caneta no papel e sobe. Em seguida move até encontrar os sensores de fim-de-trilho, e depois reposiciona em (0,0) e aproxima a caneta do papel novamente.
static void Reset(void) {
        int okCount, i;
        //Reinicia a constante de velocidade (Que pode ter sido alterada)
        //Desce a caneta e em seguida sobe novamente.
        canetaDown = 0;
        desce(400);
        sobe(400);

       //Move até a posição [0,0].
       //Como os sensores não são totalmente confiáveis, fazemos a checagem 10 vezes (okCount) antes de ter certeza que
       //Eles chegaram os fim-de-trilho.
        for (okCount=0; okCount<10; okCount++) {
                //Ainda não chegou ao fim do eixo X. Move um pouquinho mais pra lá, e reseta o OkCount
                if (!(IOPIN0 & (1<<sensores[0]))) { 
                        cur_passoX = (cur_passoX + numPassos -1) % numPassos;
                        okCount = 0;
                } 
                //Ainda não chegou ao fim do eixo Y. Move um pouquinho mais pra lá, e reseta o OkCount
                if (! (IOPIN0 & (1<<sensores[1]))) {
                        cur_passoY = (cur_passoY + numPassos -1) % numPassos;
                        okCount = 0;
                }
                atualizaMotores();
        }
        
        //Move um pouquinho pro lado-> Os sensores ficam fora da área útil!
        for (i=0; i<230; i++) {
                cur_passoY = (cur_passoY + 1) % numPassos;
                atualizaMotores();
        }
        paraMotores();        
        
        //Marca a posição atual com (0,0), e aproxima a caneta do papel.
        curX = 0;
        curY = 0;
        desce(400);
	sobe(50);
}




/*
*Looping principal: Lê os comandos, faz o parse e executa.
*/
void funciona(void) {
        //Acende a luz vermelha e apaga a verde (AGUARDE)
        IOCLR0 |= 1<<luzes[0];
        IOSET0 |= 1<<luzes[1];

        //Posiciona a caneta num lugar conhecido
        Reset();
        
        //DBG("//Esperando ordens...\n");        
        //Lê e executa os comandos
        while (1) {
		int cmd, x1, y1, x2, y2;
		if (!tiraDaFila(&cmd, &x1, &y1, &x2, &y2)) {
			//Acende a luz verde e apaga a vermelha (PRONTO)
			IOCLR0 |= 1<<luzes[1];
			IOSET0 |= 1<<luzes[0];
			sobe(50);
			continue;
		} else {
			//Acende a luz vermelha e apaga a verde (OCUPADO)
			IOSET0 |= 1<<luzes[1];
			IOCLR0 |= 1<<luzes[0];
		}

		x1 = max(0,min(maxX, x1));
		y1 = max(0,min(maxY, y1));
		x2 = max(0,min(maxX, x2));
		y2 = max(0,min(maxY, y2));

		//DBG("Executando cmd %d(%d %d %d %d) - Fila: %d\n", cmd, x1, y1, x2, y2, espacoLivreFila());

		switch (cmd) {
			case CMD_RESET: {
						Reset();
						break;
					}

			case CMD_MOVE:	{
						sobe(50);
						move (x1, y1);
						break;
					}
			case CMD_LINE:	{
						if ( (curX != x1) || (curY != y1) ) {
							//DBG("Subindo..\n");
							sobe(50);
							//DBG("Movendo..\n");
							move (x1, y1);
						}
						//DBG("Descendo..\n");
						desce(50);
						//DBG("Desenhando..\n");
						move (x2, y2);
						//DBG("Desenhado..\n");
						break;
					}
			default:	{
						//DBG("Comando Inválido: %i (%d, %d, %d, %d)\n", cmd, x1, y1, x2, y2);
						break;
					}
		}
        }
}
