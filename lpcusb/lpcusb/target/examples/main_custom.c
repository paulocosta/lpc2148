/*
	LPCUSB, an USB device driver for LPC microcontrollers	
	Copyright (C) 2006 Bertrik Sikken (bertrik@sikken.nl)

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. The name of the author may not be used to endorse or promote products
	   derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
	IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
	THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
	THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
	This is a very simple custom device (not belonging to a specific USB
	class). It implements primitive read and write in the ARM memory space.
	
	Each transfer is initiated by a control transfer to inform the device
	about the address and size of the following data transfer.
	The data transfer takes place over a bulk endpoint (BULK_IN_EP for
	reads and BULK_OUT_EP for writes).

	This example can be used to measure USB transfer speed.
*/

#include "type.h"
#include "debug.h"

#include "hal.h"
#include "console.h"
#include "usbapi.h"
#include "lpc214x.h"
#include "armVIC.h"
#include "rabiscomatico.h"

#define BULK_IN_EP		0x82
#define BULK_OUT_EP		0x05

#define MAX_PACKET_SIZE	64

#define LE_WORD(x)		((x)&0xFF),((x)>>8)




typedef struct {
      int cmd;
      int x1, y1, x2, y2;
} Comando;

Comando fila_comandos[128];
int tamTotalFilaComandos = sizeof(fila_comandos)/sizeof(Comando);
int inicioFilaComandos  = 0;//Lugar da primeira instrução da Fila
int fimFilaComandos     = 0;

void poeNaFila(int cmd, int x1, int y1, int x2, int y2) {
	fila_comandos[fimFilaComandos].cmd=cmd;
	fila_comandos[fimFilaComandos].x1=x1;
	fila_comandos[fimFilaComandos].y1=y1;
	fila_comandos[fimFilaComandos].x2=x2;
	fila_comandos[fimFilaComandos].y2=y2;
	fimFilaComandos = (fimFilaComandos+1) % tamTotalFilaComandos;
}

char tiraDaFila(int* cmd, int* x1, int* y1, int* x2, int* y2) {
	if (fimFilaComandos == inicioFilaComandos) 
		return 0; //Fila Vazia

	*cmd = fila_comandos[inicioFilaComandos].cmd;
	*x1  = fila_comandos[inicioFilaComandos].x1;
	*y1  = fila_comandos[inicioFilaComandos].y1;
	*x2  = fila_comandos[inicioFilaComandos].x2;
	*y2  = fila_comandos[inicioFilaComandos].y2;
	inicioFilaComandos = (inicioFilaComandos+1) % tamTotalFilaComandos;

	return 1;
}

int espacoLivreFila() {
	int usado = fimFilaComandos - inicioFilaComandos;
	while (usado < 0) 
		usado += tamTotalFilaComandos;

	return tamTotalFilaComandos - usado - 1;
} 


static const U8 abDescriptors[] = {

/* Device descriptor */
	0x12,              		
	DESC_DEVICE,       		
	LE_WORD(0x0200),		// bcdUSB	
	0xFF,              		// bDeviceClass
	0x00,              		// bDeviceSubClass
	0x00,              		// bDeviceProtocol
	MAX_PACKET_SIZE0,  		// bMaxPacketSize
	LE_WORD(0xFFFF),		// idVendor
	LE_WORD(0x0004),		// idProduct
	LE_WORD(0x0100),		// bcdDevice
	0x01,              		// iManufacturer
	0x02,              		// iProduct
	0x03,              		// iSerialNumber
	0x01,              		// bNumConfigurations

// configuration
	0x09,
	DESC_CONFIGURATION,
	LE_WORD(0x20),  		// wTotalLength
	0x01,  					// bNumInterfaces
	0x01,  					// bConfigurationValue
	0x00,  					// iConfiguration
	0x80,  					// bmAttributes
	0x32,  					// bMaxPower

// interface
	0x09,   				
	DESC_INTERFACE, 
	0x00,  		 			// bInterfaceNumber
	0x00,   				// bAlternateSetting
	0x02,   				// bNumEndPoints
	0xFF,   				// bInterfaceClass
	0x00,   				// bInterfaceSubClass
	0x00,   				// bInterfaceProtocol
	0x00,   				// iInterface

// bulk in
	0x07,   		
	DESC_ENDPOINT,   		
	BULK_IN_EP,				// bEndpointAddress
	0x02,   				// bmAttributes = BULK
	LE_WORD(MAX_PACKET_SIZE),// wMaxPacketSize
	0,						// bInterval   		

// bulk out
	0x07,   		
	DESC_ENDPOINT,   		
	BULK_OUT_EP,			// bEndpointAddress
	0x02,   				// bmAttributes = BULK
	LE_WORD(MAX_PACKET_SIZE),// wMaxPacketSize
	0,						// bInterval   		

// string descriptors
	0x04,
	DESC_STRING,
	LE_WORD(0x0409),

	// manufacturer string
	0x0E,
	DESC_STRING,
	'L', 0, 'P', 0, 'C', 0, 'U', 0, 'S', 0, 'B', 0,

	// product string
	0x1A,
	DESC_STRING,
	'M', 0, 'e', 0, 'm', 0, 'o', 0, 'r', 0, 'y', 0, 'A', 0, 'c', 0, 'c', 0, 'e', 0, 's', 0, 's', 0,

	// serial number string
	0x12,
	DESC_STRING,
	'D', 0, 'E', 0, 'A', 0, 'D', 0, 'C', 0, '0', 0, 'D', 0, 'E', 0,
	
	// terminator
	0
};

static void _HandleBulkIn(U8 bEP, U8 bEPStatus)
{
	unsigned char buf[] =
			{ 
				espacoLivreFila()
				//(exCount>>24)&0xFF,
				//(exCount>>16)&0xFF
				//(exCount>> 8)&0xFF
				//(exCount>> 0)&0xFF
			};
	

	//Manda o tamanho livre da fila de instruções
	USBHwEPWrite(bEP, buf , sizeof(buf));
}


static void _HandleBulkOut(U8 bEP, U8 bEPStatus)
{
	unsigned char buf[56];
	int len = USBHwEPRead(bEP, buf, sizeof(buf) );
	
	if (len > 0) {
		int cmd = buf[0];
		int x1=0, y1=0, x2=0, y2=0;
		
		if (len >= 5) {
			x1 = buf[1]<<8 | buf[2];
			y1 = buf[3]<<8 | buf[4];
			if (len >= 9) {
				x2 = buf[5]<<8 | buf[6];
				y2 = buf[7]<<8 | buf[8];
			}
		}
		
		if (cmd == CMD_QUEUE_LEN) {
			_HandleBulkIn (BULK_IN_EP, 0);
		} else {
/*
			if (IOPIN0 & (1<<18)) {
				IOCLR0 = 1<<18;
			} else {
				IOSET0 = 1<<18;
			}*/
			poeNaFila(cmd, x1, y1, x2, y2);
		}
	}
}


/*************************************************************************
	HandleVendorRequest
	===================
		Handles vendor specific requests
		
	Control transfer fields:
	* request:	0x01 = prepare memory read
				0x02 = prepare memory write
	* index:	ignored
	* value:	ignored
	* data:		U32 dwAddress
				U32 dwLength
		
**************************************************************************/
static BOOL HandleVendorRequest(TSetupPacket *pSetup, int *piLen, U8 **ppbData)
{
	switch (pSetup->bRequest) {
	
	// prepare read
	case 0x01:
		break;
		
	// prepare write
	case 0x02:
		break;

	default:
		//DBG("Unhandled class %X\n", pSetup->bRequest);
		return FALSE;
	}
	return TRUE;
}




#define BAUD_RATE	38400


/*************************************************************************
	main
	====
**************************************************************************/
static void USBIntHandler(void) __attribute__ ((interrupt("IRQ")));
static void USBIntHandler(void)
{
	USBHwISR();
	VICVectAddr = 0x00;    // dummy write to VIC to signal end of ISR 	
}

void initUSB() {
	//DBG("Initialising USB stack\n");

	// initialise stack
	USBInit();

	// register device descriptors
	USBRegisterDescriptors(abDescriptors);

	// register HID standard request handler
	//USBRegisterCustomReqHandler(HIDHandleStdReq);

	// register class request handler
	//USBRegisterRequestHandler(REQTYPE_TYPE_CLASS, HandleClassRequest, abClassReqData);

	// register endpoint
	USBHwRegisterEPIntHandler(BULK_IN_EP, NULL);
	USBHwRegisterEPIntHandler(BULK_OUT_EP, _HandleBulkOut);

	// register frame handler
	//USBHwRegisterFrameHandler(HandleFrame);




/*	
	
	// register device descriptors
	USBRegisterDescriptors(abDescriptors);

	// override standard request handler
	//USBRegisterRequestHandler(REQTYPE_TYPE_VENDOR, HandleVendorRequest, abVendorReqData);

	// register endpoints
	USBHwRegisterEPIntHandler(BULK_IN_EP, _HandleBulkIn);
	USBHwRegisterEPIntHandler(BULK_OUT_EP, _HandleBulkOut);

	DBG("Starting USB communication\n");
*/

	//Fazendo USB por interrupção
	(*(&VICVectCntl0+0)) = 0x20 | 22; // choose highest priority ISR slot 	
	(*(&VICVectAddr0+0)) = (int)USBIntHandler;
	VICIntSelect &= ~(1<<22);               // select IRQ for USB
	VICIntEnable |= (1<<22);

}

int main(void)
{
	//Configura pinos de saída
        char pinos_out[]= {18,19, 4, 5,
                            6, 7, 8, 9,
                           10,16,12,13};
        int i;
        for (i=0; i<sizeof(pinos_out); i++) {
                IODIR0  |= 1<<pinos_out[i];
        }


	// PLL and MAM
	HalSysInit();

	// init DBG
	ConsoleInit(60000000 / (16 * BAUD_RATE));


	initUSB();
	enableIRQ();
	USBHwConnect(TRUE);

	// call USB interrupt handler continuously
	funciona();
	
	return 0;
}

