/*
        Paulo Costa - Set/2008
        
        Programinha besta que redireciona stdin/stdout para uma porta serial.
        A porta é aberta a 38400, que é velocidade usada pelo nosso brinquedo.
        O Dispositivo que deverá ser usado é especificado como primeiro (E único) 
        argumento do programa.
        
        Para compilar:
        gcc IO.c -o IO -lpthread
        
        Para executar:
        ./IO /dev/ttyUSB0
        
        Código baseado em: 
                "Serial Port Tester" - Doug Hughes - Auburn University College of Engineering
                "lpc2k_pgm" - Paul Stoffregen - http://www.pjrc.com/arm/lpc2k_pgm
        
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termio.h>
#include <sys/fcntl.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <linux/serial.h>
#include <pthread.h>

int fd;

//Liga ou desliga o sinal DTR. 
//No nosso caso, este sinal está ligado ao reset do microcontrolador
void set_dtr(int val)
{
        int flags;

        ioctl(fd, TIOCMGET, &flags);
        if (val) {
                flags |= TIOCM_DTR;
        } else {
                flags &= ~(TIOCM_DTR);
        }
        ioctl(fd, TIOCMSET, &flags);
}

//Ajusta a velocidade da porta para o valor especificado (No nosso caso, B38400)
int set_baud(tcflag_t baud)
{
        struct termios port_setting;
        int r;

        if (fd < 0) {
                return -1;
        }
        if (baud == B0) {
                return -2;
        }
        r = tcgetattr(fd, &port_setting);
        if (r != 0)  {
                return -3;
        }
        //port_setting.c_iflag = IGNBRK | IGNPAR | IXANY | IXON;
        port_setting.c_iflag = IGNBRK | IGNPAR;
        port_setting.c_cflag = baud | CS8 | CREAD | HUPCL | CLOCAL;
        port_setting.c_oflag = 0;
        port_setting.c_lflag = 0;
        r = tcsetattr(fd, TCSAFLUSH, &port_setting);
        
        if (r != 0) {
                return -4;
        }
        return 0;
}

//Abre a porta especificada, na velocidade especificada
void open_serial_port(const char *port_name, tcflag_t baud)
{
        int r;
        struct serial_struct kernel_serial_settings;

        fd = open(port_name, O_RDWR);
        set_baud(baud);
        
        /* attempt to set low latency mode, but don't worry if we can't */
        r = ioctl(fd, TIOCGSERIAL, &kernel_serial_settings);
        if (r < 0) return;
        kernel_serial_settings.flags |= ASYNC_LOW_LATENCY;
        ioctl(fd, TIOCSSERIAL, &kernel_serial_settings); 
}

//Flag que indica quando o programa deve terminar
char rodando = 1;

//Thread que Redireciona todos os dados da entrada padrão para a porta serial.
//Os dados são enviados uma linha por vez.
void* stdin2tx(void * a) {
        char buffer[1024]; //Buffer de leitura
        while (rodando) {
                int ret = scanf(" %[^\n]s", buffer);
                //Acabou o stream... Tá na hora de terminar o processo!
                if (ret == EOF)  {
                        rodando = 0;
                //Temos dados para mandar pra serial!
                } else if (ret > 0) {
                        write(fd, buffer, strlen(buffer));
                        write(fd, "\n", 1);        
                //Nada pra ler... Esperamos um pouquinho pra poupar a CPU e tentamos de novo.
                } else {
                        usleep(10000);
                }
        }

        pthread_exit(NULL);
}

//Thread que Redireciona todos os dados lidos da porta serial para a saída padrão.
void* rx2stdout(void* a) {
        int i, len;
        char io[BUFSIZ];
        
        while (rodando) {
                len = read(fd, io, BUFSIZ);
                
                //Se não há nada pra ler, podemos dar um descanso para a CPU.
                if (len <= 0) {
                        usleep(10000);
                } else {
                        for (i=0; i<len; i++) {
                                printf("%c", io[i]);
                        }
                        //Como a comunicação é iterativa, é importante dar FLUSH no fim de uma escrita, ou temos um deadlock.
                        fflush(stdout);
                }
        }
        pthread_exit(NULL);
}



//Método principal: Abre a porta, reseta o dispositivo e inicia as threads de comunicação.
main(int argc, char *argv[]) 
{       
        pthread_t threadRX;
        pthread_t threadTX;

        //Abre a porta
        open_serial_port(argv[1], B38400);

        //Força um RESET
        set_dtr(1);
        usleep(100000);
        printf("//Resetando...\n");
        set_dtr(0);

        //Inicia as thread de comunicação
        pthread_create(&threadRX, NULL, rx2stdout, NULL);
        pthread_create(&threadTX, NULL, stdin2tx, NULL);

        pthread_exit(NULL);
}
