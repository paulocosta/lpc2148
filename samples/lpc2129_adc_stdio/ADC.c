/******************************************************************************
 *
 * LPC2119 Demo - stdio with newlib and analog to digital conversion
 * by Martin THOMAS <eversmith@heizung-thomas.de>
 *
 * Reads voltage at PINA0 in "Singe-Conversion/Burst-Mode"
 * and uses stdio/printf to view the result.
 *
 * ----------------------------------------------------------------------------
 *
 * - UART functions based on code from Bill Knight (R O Software)
 * - newlib stdio-interface from newlib-lpc (Aeolus Development)
 *   (may be easier to port to other targets in this form)
 * - V/V3A->V conversion based on an example found somewhere in the net
 *
 * ----------------------------------------------------------------------------
 *
 *  Tested with WinARM 4/05 (arm-elf-gcc 4.0.0) and 
 *  a Philips LPC2129 on a LPC-P2129 board (Olimex)
 *
 *  Review 20060710: 
 *  - updated makefile
 *  - test with WinARM 6/06 (arm-elf-gcc 4.1.1)
 *  Reviews 200603**
 *  - added fixes/improvements provided by Alexey Shusharin 
 *
 *****************************************************************************/

/*
	With arm-elf-gcc 4.0.0, newlib 1.13, Thumb-mode:
	section            size         addr
	.text             12939            0
	.data              2088   1073741824
	.bss                 68   1073743912
	.stack             1024   1073744128
	
	ARM-Mode:
	section            size         addr
	.text             18011            0
	
	With arm-elf-gcc 4.1.1, newlib 1.14, updated makefile
	             section            size         addr
	Thumb-mode:  .text             11208            0
	ARM-mode:    .text             15692            0
*/

#include "lpc21xx.h"
#include <stdio.h>

#include "config.h" /* Clock config */
#include "uart.h"

#define BAUD 115200

/* Voltage at V_ddA/V3A (LPC2129 Pin7) */
/* connected to 3,3V on LPC-P2129      */
#define VREF  33/10  

static void systemInit(void)
{
	// --- enable and connect the PLL (Phase Locked Loop) ---
	// a. set multiplier and divider
	PLLCFG = MSEL | (0<<PSEL1) | (1<<PSEL0);
	// b. enable PLL
	PLLCON = (1<<PLLE);
	// c. feed sequence
	PLLFEED = PLL_FEED1;
	PLLFEED = PLL_FEED2;
	// d. wait for PLL lock (PLOCK bit is set if locked)
	while (!(PLLSTAT & (1<<PLOCK)));
	// e. connect (and enable) PLL
	PLLCON = (1<<PLLE) | (1<<PLLC);
	// f. feed sequence
	PLLFEED = PLL_FEED1;
	PLLFEED = PLL_FEED2;
	
	// --- setup and enable the MAM (Memory Accelerator Module) ---
	// a. start change by turning of the MAM (redundant)
	MAMCR = 0;	
	// b. set MAM-Fetch cycle to 3 cclk as recommended for >40MHz
	MAMTIM = MAM_FETCH;
	// c. enable MAM 
	MAMCR = MAM_MODE;
	
	// --- set VPB speed ---
	VPBDIV = VPBDIV_VAL;
	
	// --- map INT-vector ---
	#if defined(RAM_RUN)
	  MEMMAP = MEMMAP_USER_RAM_MODE;
	#elif defined(ROM_RUN)
	  MEMMAP = MEMMAP_USER_FLASH_MODE;
	#else
	#error RUN_MODE not defined!
	#endif
}

static void delay(void )
{
	volatile int i,j;

	for (i=0;i<2000;i++)
		for (j=0;j<1000;j++);
}

int main(void) 
{
	unsigned int val, chan;
	char s[21] = "12345678901234567890";
	
	systemInit(); /* init PLL, MAM etc. */

	uart0Init(UART_BAUD(BAUD), UART_8N1, UART_FIFO_8);
	
	uart0Puts("\r\nHallo (uart0Puts)\r\n");
	iprintf("Hallo (printf)\n");
	
	iprintf("Type something: ");
	fgets(s,20+1,stdin);
	iprintf("\nhere it is again: %s\n\n",s);

	/* Setup A/D: 10-bit AIN0 @ 4,2MHz "Non-Burst"-Mode */
	
	PINSEL1 |= (1UL<<22); // set function P0.27 as AIN0
	
	// sample AIN0 only => bit 0 = 1 
	// CLKDIV = 14 (59/14 = 4.21 < 4.5 MHz) => Bits 8ff = 14-1
	// BURST = 1 => set Bit 16 - wuff: disabled below
	// PDN   = 1 => set Bit 21
	ADCR = ( 1 | ((14-1)<<8) /*| (1UL<<16)*/ | (1UL<<21) );
	
	/* TODO: check manual again: "BURST" & "Start A/D"  */
	
	while(1) {

		ADCR  |= (1UL<<24);  /* Start A/D Conversion (START:0=1) */
		while ((ADDR & (1UL<<31)) == 0); /* Wait for the conversion to complete (DONE=1)*/
		val = ((ADDR >> 6) & 0x03FF);	/* Extract the A/D result */
		chan = ((ADDR >> 24) &0x0007); /* Channel (should be 0)  */
		
		iprintf ("AIN%i: Digital Value %4u = %01u.%04u Volts\n",
			 chan, (unsigned) val,
			 (unsigned) (val * VREF) >> 10,                          /* Output Integer Portion */
			 (unsigned) ((val * VREF * 10000UL) >> 10UL) % 10000);   /* Output Decimal Portion */
		
		delay();

	} // while
	
	return 0; /* never reached */
}
