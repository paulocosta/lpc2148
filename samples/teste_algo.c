#include <stdio.h>

int curX=0, curY=0;
int cur_passoX=0, cur_passoY=0;
char canataDown = 0;


int pinos_motorCC[] = {3,4};
int pinos_motorX[] = { 6, 7, 8, 9};
int pinos_motorY[] = {10,16,12,13};

//Tabela de comandos para "meio-passo"
int passos[] = { 0x1,    //---X
                 0x3,    //--XX
                 0x2,    //--X-
                 0x6,    //-XX-
                 0x4,    //-X--
                 0xC,    //XX--
                 0x8,    //X---
                 0x9     //X--X
                };
#define numPassos (sizeof(passos)/sizeof(passos[0]))

#define sinal(a) (a>0?1:-1)
#define abs(a)   (a>0?a:-a)

void paraMotores() {
        int clearFlags = 0;
        int i;
        for (i=0; i<4; i++) {
                clearFlags |= 1<<pinos_motorX[i];
                clearFlags |= 1<<pinos_motorY[i];
        }
}

void atualizaMotores() {
        int setFlags = 0;
        int clearFlags = 0;
        int i;

        for (i=0; i<4; i++) {
                if (passos[cur_passoX] & (1<<i)) {
                        setFlags   |= 1<<pinos_motorX[i];
                } else {
                        clearFlags |= 1<<pinos_motorX[i];
                }
                        
                if (passos[cur_passoY] & (1<<i)) {
                        setFlags   |= 1<<pinos_motorY[i];
                } else {
                        clearFlags |= 1<<pinos_motorY[i];
                }
        }


        //Começa a viadagem... Daqui pra baixo só serve para exibir os pinos.
        printf("//[%i, %i] - ", curX, curY);

        for (i=0; i<4; i++) {
                if (setFlags & 1<<pinos_motorX[i]) {
                        printf("%3i", pinos_motorX[i]);
                } else {
                        printf("   ");
                }
        }
        
        printf("  -  ");
        
        for (i=0; i<4; i++) {
                if (setFlags & 1<<pinos_motorY[i]) {
                        printf("%3i", pinos_motorY[i]);
                } else {
                        printf("   ");
                }
        }
        printf("\n");
}

void move_aux (int* x, int* passoX, int* y, int* passoY, int deslocX, int deslocY) {
        if (abs(deslocY) > abs(deslocX)) {
                move_aux(y, passoY, x, passoX, deslocY, deslocX);
                return;
        }

        //Por praticidade, vamos trabalhar com os módulos de dX e dY. O sentido é indicado por dirX e dirY
        int dirX = sinal(deslocX);
        int dirY = sinal(deslocY);
        
        int deslocAtualX=0;
        int deslocAtualY=0;
        
        deslocX = abs(deslocX);
        deslocY = abs(deslocY);
        
        for (deslocAtualX=0; deslocAtualX<deslocX;) {
                deslocAtualX++;
                *x += dirX;
                *passoX = (*passoX + numPassos + dirX) % numPassos;
                
        
                //Só desloca em Y se    deslocAtual x desloc > 0 <=> Posição Atual está Abaixo da desejada
                int crossProductSemDesloc = deslocAtualX*deslocY - (deslocAtualY  )*deslocX;
                int crossProductComDesloc = deslocAtualX*deslocY - (deslocAtualY+1)*deslocX;
                
                //printf("sem: %i\ncom:%i\n", crossProductSemDesloc, crossProductComDesloc);
                if (abs(crossProductComDesloc) < abs(crossProductSemDesloc)) {
                        deslocAtualY++;
                        *y += dirY;
                        *passoY = (*passoY + numPassos + dirY) % numPassos;
                }
                
                atualizaMotores();
        }
        paraMotores();
}

void move(int destX, int destY) {
        int deslocX = destX-curX;
        int deslocY = destY-curY;                

        move_aux(&curX, &cur_passoX, &curY, &cur_passoY, deslocX, deslocY);
}

int leLinha(char** palavras) {
        int palavra = 0;
        int index = 0;               

        while (1) {
                char c = getchar();
                
               
                //ESPAÇO ==> pula pra próxima palavra
                if (c <= ' ') {
                        if (index != 0) {
                                palavras[palavra][index] = 0;
                                palavra++;
                                index = 0;
                        }


                //ENTER ==> retorna
                if ((c == 13) || (c == 10)) {
                        palavras[palavra][index] = '\0';
                        if (index == 0) 
                                palavra--;
                        return palavra+1;
                }



                //Letrinhas de verdade
                } else {                        
                        //Passa pra uppercase
                        if (c >='a' && c<='z') {
                                c += 'A' -'a';
                        }
                                
                        //só considera A-Z, 0-9
                        if ( (c >='A' && c<='Z') || 
                             (c >='0' && c<='9') )
                        {
                                palavras[palavra][index++] = c;
                        }
                }
        }
}






void sobe() {
}

void desce() {
}







int main() {
        
        while (1) {
                char palavras[20][30];
                char* ptr[20];
                int i;
                for (i=0; i<20; i++) { 
                        ptr[i] = palavras[i];
                }
                int numPalavras = leLinha(ptr);
                printf("//");
                for (i=0; i<numPalavras; i++) { 
                        printf("%s", palavras[i]);
                        if (i<numPalavras-1) {
                                printf(" ");
                        } else  {
                                printf("\n");
                        }
                }
                
                
                if (numPalavras == 0)  {
                        printf("ERRO - Nenhum comando\n");
                        continue;
                }
                
                if (strcmp (palavras[0], "UP") == 0) {
                        if (numPalavras == 1) {
                                sobe();
                                printf("OK - Subiu\n");
                        } else {
                                printf("ERRO - Numero de parametros incorreto\n");
                        }
                        
                        continue;
                }

                if (strcmp (palavras[0], "DOWN") == 0) {
                        if (numPalavras == 1) {
                                desce();
                                printf("OK - Desceu\n");
                        } else {
                                printf("ERRO - Numero de parametros incorreto\n");
                        }
                        
                        continue;
                }

                if (strcmp (palavras[0], "MOVE") == 0) {
                        if (numPalavras != 3) {
                                printf("ERRO - Numero de parametros incorreto\n");
                        } else {
                                int x, y;
                                if        (sscanf(palavras[1], "%i", &x) != 1) {
                                        printf("ERRO - valor X não é um numero\n");
                                } else if (sscanf(palavras[2], "%i", &y) != 1) {
                                        printf("ERRO - valor Y não é um numero\n");
                                } else {
                                        move (x, y);
                                        printf("OK - Moveu para [%i, %i]\n", x, y);
                                }
                        }
                        
                        continue;
                }
                
                //Default: Comando inválido
                printf("ERRO - Comando desconhecido: '%s'\n", palavras[0]);
                continue;
/*


                int destX, destY;
                scanf("%i%i", &destX, &destY);
                
                int deslocX = destX-x;
                int deslocY = destY-y;                

                move (&x, &passoX, &y, &passoY, deslocX, deslocY);
                */
        }
}
