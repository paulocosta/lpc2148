/******************************************************************************
 *
 * LPC2119 Demo - stdio with newlib and analog to digital conversion
 * by Martin THOMAS <eversmith@heizung-thomas.de>
 *
 * Reads voltage at PINA0 in "Singe-Conversion/Burst-Mode"
 * and uses stdio/printf to view the result.
 *
 * ----------------------------------------------------------------------------
 *
 * - UART functions based on code from Bill Knight (R O Software)
 * - newlib stdio-interface from newlib-lpc (Aeolus Development)
 *   (may be easier to port to other targets in this form)
 * - V/V3A->V conversion based on an example found somewhere in the net
 *
 * ----------------------------------------------------------------------------
 *
 *  Tested with WinARM 4/05 (arm-elf-gcc 4.0.0) and 
 *  a Philips LPC2129 on a LPC-P2129 board (Olimex)
 *
 *  Review 20060710: 
 *  - updated makefile
 *  - test with WinARM 6/06 (arm-elf-gcc 4.1.1)
 *  Reviews 200603**
 *  - added fixes/improvements provided by Alexey Shusharin 
 *
 *****************************************************************************/

/*
	With arm-elf-gcc 4.0.0, newlib 1.13, Thumb-mode:
	section            size         addr
	.text             12939            0
	.data              2088   1073741824
	.bss                 68   1073743912
	.stack             1024   1073744128
	
	ARM-Mode:
	section            size         addr
	.text             18011            0
	
	With arm-elf-gcc 4.1.1, newlib 1.14, updated makefile
	             section            size         addr
	Thumb-mode:  .text             11208            0
	ARM-mode:    .text             15692            0
*/

#include "algoritmo.h"
#include <stdio.h>

#include "config.h" /* Clock config */
#include "uart.h"

#define BAUD 38400

/* Voltage at V_ddA/V3A (LPC2129 Pin7) */
/* connected to 3,3V on LPC-P2129      */
#define VREF  33/10  


/* 
 *  Inicializa os perif�ricos principais:
 *    PLL - Multiplica a frequencia do cristal para conseguir rodar a 60MHz
 *    MAM - M�dulo acelerador de mem�ria - Aumenta o desempenho fazendo pr�-fetch da mem�ria.
 */
static void systemInit(void)
{
	// --- enable and connect the PLL (Phase Locked Loop) ---
	// a. set multiplier and divider
	PLLCFG = MSEL | (0<<PSEL1) | (1<<PSEL0);
	// b. enable PLL
	PLLCON = (1<<PLLE);
	// c. feed sequence
	PLLFEED = PLL_FEED1;
	PLLFEED = PLL_FEED2;
	// d. wait for PLL lock (PLOCK bit is set if locked)
	while (!(PLLSTAT & (1<<PLOCK)));
	// e. connect (and enable) PLL
	PLLCON = (1<<PLLE) | (1<<PLLC);
	// f. feed sequence
	PLLFEED = PLL_FEED1;
	PLLFEED = PLL_FEED2;
	
	// --- setup and enable the MAM (Memory Accelerator Module) ---
	// a. start change by turning off the MAM (redundant)
	MAMCR = 0;	
	// b. set MAM-Fetch cycle to 3 cclk as recommended for >40MHz
	MAMTIM = MAM_FETCH;
	// c. enable MAM 
	MAMCR = MAM_MODE;
	
	// --- set VPB speed ---
	VPBDIV = VPBDIV_VAL;
	
        
	// --- map INT-vector ---
	#if defined(RAM_RUN)
	  MEMMAP = MEMMAP_USER_RAM_MODE;
	#elif defined(ROM_RUN)
	  MEMMAP = MEMMAP_USER_FLASH_MODE;
	#else
	#error RUN_MODE not defined!
	#endif
}


/*
 * M�todo principal. Inicializa os perif�ricos principais, a porta serial e os pinos de entrada e sa�da.
 * Depois de configurar tudo, chama "funciona()" no arquivo algoritmo.c, que cont�m a l�gica de funcionamento.
 */
int main(void) 
{
        //Lista de pinos que devem ser configurados como "OUT". (Padr�o � "IN")
        char pinos_out[]= {18,19, 4, 5,
                            6, 7, 8, 9,
                           10,16,12,13};
        int dirMask = 0; //Variav�l tempor�ria com a mascara de Entrada/Saida.
        int i; //Contador Gen�rico
        
        
         /* inicia PLL, MAM etc. */       
	systemInit();
             
        /* inicia a Serial */
        uart0Init(UART_BAUD(BAUD), UART_8N1, UART_FIFO_8);        
        
        /*Configura os pinos de saida*/
        for (i=0; i<sizeof(pinos_out); i++) {
                dirMask |= 1<<pinos_out[i];
        }
        IODIR0 = dirMask;
        
        
        //Para de inicializar e funciona!!
        funciona();
        
	
	return 0; /* never reached */
}
