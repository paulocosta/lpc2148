/******************************************************************************
 *
 * Paulo Costa
 *
 * Arquivo principal contendo a lógica de controle do plotter.
 * Essa parte do programa se preocupa minimamente com o dispositivo utilizado, 
 * se concentrando na lógica de funcionamento.
 *
 * Com exceção deste arquivo, todo o resto do programa foi quase totalmente copiado 
 * dos projetos de Martin Thomas, em especial, usamos como base 
 * "Newlib/StdIO-Interface and LPC ADC example", disponível em:
 * http://gandalf.arubi.uni-kl.de/avr_projects/arm_projects/#gcc_stdio
 * 
 *****************************************************************************/




#include <stdio.h>
#include <uart.h>
#include <string.h>
#include "algoritmo.h"

//Posição Atual da caneta no papel
int curX = 0;
int curY = 0;
//Posição Atual dos passos dos motores
int cur_passoX = 0;
int cur_passoY = 0;
//Tamanho da área útil do papel. Evita que os motores tentem ultrapassar os limites e comecem a fazer barulhos feitos.
int maxX = 2750;
int maxY = 930;
//Indica se a caneta está encostada no papel
char canetaDown = 0;
//Constante de tempo usada nos "Delays"
int delayconst = 30000;

//////////// PINOS //////////////

//Pinos dos sensores de fim-de-trilho nos eixos X e Y
char sensores[]      = {30, 20};
//Pinos das luzes VERDE e VERMELHA
char luzes[]         = {18, 19};
//Pinos do motor CC que sobe/desce a caneta. 
//Ligar apenas o primeiro pino faz com que ele desça, ligar apenas o segunda faz com que ele suba.
char pinos_motorCC[] = { 4, 5}; //[desce, sobe]
//Pinos responsáveis por acionar as fases do motor X
char pinos_motorX [] = { 9, 8, 7, 6};
//Pinos responsáveis por acionar as fases do motor Y
char pinos_motorY [] = {10,16,12,13};

//Tabela de comandos para "meio-passo"
char passos[] = { 0x1,    //---X
                 0x3,    //--XX
                 0x2,    //--X-
                 0x6,    //-XX-
                 0x4,    //-X--
                 0xC,    //XX--
                 0x8,    //X---
                 0x9     //X--X
                };
#define numPassos sizeof(passos)

#define sinal(a) (a>0?1:-1)
#define abs(a)   (a>0?a:-a)

//Pára a execução por N unidade de tempo.
//A unidade de tempo é definida por "delayconst".
static void delay(int n) {
        int i,j;
        for (i=0; i<n; i++)
                for (j=0; j<delayconst; j++);
}

//Desliga os motores de passo ao fim de um movimento.
//Isso serve para poupar um pouco de potência... Mas seria dispensável.
static void paraMotores(void) {
        int clearFlags = 0;
        int i;
        for (i=0; i<4; i++) {
                clearFlags |= 1<<pinos_motorX[i];
                clearFlags |= 1<<pinos_motorY[i];
        }
        
        delay(2);
        IOCLR0 |= clearFlags;
}

//Liga/desliga as fases dos motores de passo de acordo com o passo desejado.
static void atualizaMotores(void) {
        //Listas de pinos que precisam ser ligados/desligados
        int setFlags = 0;
        int clearFlags = 0;
        int i;

        //Para cada um dos 4 pinos de cada motor
        for (i=0; i<4; i++) {
                //Verificamos a tabela de passos para o passo atual do motor X, e ligamos ou desligamos o 
                //i-ézimo pino deste de acordo com o valor lido na tabela.
                if (passos[cur_passoX] & (1<<i)) {
                        setFlags   |= 1<<pinos_motorX[i];
                } else {
                        clearFlags |= 1<<pinos_motorX[i];
                }
                
                //Mesma coisa para o motor no eixo Y
                if (passos[cur_passoY] & (1<<i)) {
                        setFlags   |= 1<<pinos_motorY[i];
                } else {
                        clearFlags |= 1<<pinos_motorY[i];
                }
        }
        
        //Liga/desliga os pinos determinados
        IOCLR0 |= clearFlags;
        IOSET0 |= setFlags;
        //Espera um pouquinho... Sem um intervalo após os movimentos, teríamos perda de passo.
        delay(1);
}



/*
 * Método auxiliar, implementa uma versão improvisada do algoritmo de Bresenham para desenho de linhas.
 * O algoritmo consiste em percorrer o eixo com maior deslocamento. A cada iteração, determina-se se o
 * outro eixo fica mais próximo da "linha ideal" na posição atual ou ao avançar em uma posição.
 *
 * Aqui, supomos que o eixo X sempre tem o maior deslocamento. Se não for o caso, a primeira coisa que 
 * o procedimento faz é trocar os eixos X e Y, para que isso seja verdade. Devido à essa gambiarra, lidamos 
 * sempre com ponteiros, ao invés de lidar com as variáveis globais
 */
static void move_aux (int* x, int* passoX, int* y, int* passoY, int deslocX, int deslocY) {
        if (abs(deslocY) > abs(deslocX)) {
                move_aux(y, passoY, x, passoX, deslocY, deslocX);
                return;
        }

        //Por praticidade, vamos trabalhar com os módulos de dX e dY. O sentido é indicado por dirX e dirY
        int dirX = sinal(deslocX);
        int dirY = sinal(deslocY);
        
        //Quanto o já moveu
        int deslocAtualX=0;
        int deslocAtualY=0;
        
        //Quanto precisa mover
        deslocX = abs(deslocX);
        deslocY = abs(deslocY);
        
        //Anda de 1 em 1 no eixo X
        for (deslocAtualX=0; deslocAtualX<deslocX;) {
                //Moveu um no eixo X
                deslocAtualX++; 
                //Manda o incremento/Decremento para a posição absoluta
                *x += dirX; 
                //Incrementa/Decrementa o passo para que o motor reflita o deslocamento
                *passoX = (*passoX + numPassos + dirX) % numPassos; 
                
        
                //Produto vetorial pode ser usado como uma medida indireta de angulo.
                //Então, fazemos 2 produtos vetoriais: Mantendo o Y no valor atual, e mudando-o.
                int crossProductSemDesloc = deslocAtualX*deslocY - (deslocAtualY  )*deslocX;
                int crossProductComDesloc = deslocAtualX*deslocY - (deslocAtualY+1)*deslocX;
                
                //E se o angulo ao Yeslocar X for menor, fazemos o deslocamento
                if (abs(crossProductComDesloc) < abs(crossProductSemDesloc)) {
                        //Moveu um no eixo Y
                        deslocAtualY++;
                        //Manda o incremento/Decremento para a posição absoluta
                        *y += dirY;
                        //Incrementa/Decrementa o passo para que o motor reflita o deslocamento
                        *passoY = (*passoY + numPassos + dirY) % numPassos;
                }
                //Atualiza os motores reais para que reflitam o deslocamento calculado.
                atualizaMotores();
        }
        //Acabou de mover, pára os motores.
        paraMotores();
}

//Move os motores para a posição X,Y especificada
static void move(int destX, int destY) {
        int deslocX = destX-curX;
        int deslocY = destY-curY;                

        //Terceiriza para o algoritmo de linha
        move_aux(&curX, &cur_passoX, &curY, &cur_passoY, deslocX, deslocY);
}




/*
Lê uma linha de comando da porta serial, e quebra ela em várias palavras, retornando o numero de palavras lidas.
*/
static int leLinha(char** palavras) {
        //Indice da palavra sendo lida
        int palavra = 0;
        //Indice do caractere da palavra sendo lida
        int charIndex = 0;               

        //Lê e processa os caracteres.
        while (1) {
                int c;
                //Verifica a porta até ter alguma coisa pra ler.
                do {
                        c = uart0Getch();
                } while (c == -1);
                
                //ESPAÇO ==> pula pra próxima palavra
                if (c <= ' ') {
                        if (charIndex != 0) {
                                //Termina a palavra com '\0'
                                palavras[palavra][charIndex] = 0;
                                //Pula pro começo da próxima palavra
                                palavra++;
                                charIndex = 0;
                        }
                //ENTER ==> retorna
                if ((c == 13) || (c == 10)) {
                        //Termina a palavra atual
                        palavras[palavra][charIndex] = '\0';
                        //Se a palavra atual tava vazia, descarta
                        if (charIndex == 0) 
                                palavra--;
                        //Retorna o numero de palavras lidas.
                        return palavra+1;
                }


                //Letrinhas de verdade
                } else {                        
                        //Passa pra uppercase
                        if (c >='a' && c<='z') {
                                c += 'A' -'a';
                        }
                                
                        //só considera A-Z, 0-9
                        if ( (c >='A' && c<='Z') || 
                             (c >='0' && c<='9') )
                        {
                                //Coloca no fim da palavra atual
                                palavras[palavra][charIndex++] = c;
                        }
                }
        }
}





//Liga o motor CC para que a caneta suba, durate "sleep" unidades de tempo
static void sobe(int sleep) {
        if (canetaDown) {
                canetaDown = 0;
                //Começa a subir
                IOSET0 |= 1<<pinos_motorCC[1];
                //espera um pouquinho
                delay(sleep);
                //Pára
                IOCLR0 |= 1<<pinos_motorCC[1];
        }
}

//Liga o motor CC para que a caneta desça, durate "sleep" unidades de tempo
static void desce(int sleep) {
        if (!canetaDown) {
                canetaDown = 1;
                //Começa a descer
                IOSET0 |= 1<<pinos_motorCC[0];
                //espera um pouquinho
                delay(sleep);
                //Pára
                IOCLR0 |= 1<<pinos_motorCC[0];
        }        
}

//Encosta a caneta no papel e sobe. Em seguida move até encontrar os sensores de fim-de-trilho, e depois reposiciona em (0,0) e aproxima a caneta do papel novamente.
static void Reset(void) {
        int okCount, i;
        //Reinicia a constante de velocidade (Que pode ter sido alterada)
        delayconst = 20000;
        //Desce a caneta e em seguida sobe novamente.
        canetaDown = 0;
        desce(900);
        sobe(950);

       //Move até a posição [0,0].
       //Como os sensores não são totalmente confiáveis, fazemos a checagem 10 vezes (okCount) antes de ter certeza que
       //Eles chegaram os fim-de-trilho.
        for (okCount=0; okCount<10; okCount++) {
                //Ainda não chegou ao fim do eixo X. Move um pouquinho mais pra lá, e reseta o OkCount
                if (!(IOPIN0 & (1<<sensores[0]))) { 
                        cur_passoX = (cur_passoX + numPassos -1) % numPassos;
                        okCount = 0;
                } 
                //Ainda não chegou ao fim do eixo Y. Move um pouquinho mais pra lá, e reseta o OkCount
                if (! (IOPIN0 & (1<<sensores[1]))) {
                        cur_passoY = (cur_passoY + numPassos -1) % numPassos;
                        okCount = 0;
                }
                atualizaMotores();
        }
        
        //Move um pouquinho pro lado-> Os sensores ficam fora da área útil!
        for (i=0; i<230; i++) {
                cur_passoY = (cur_passoY + 1) % numPassos;
                atualizaMotores();
        }
        paraMotores();        
        
        //Marca a posição atual com (0,0), e aproxima a caneta do papel.
        curX = 0;
        curY = 0;
        desce(900);
}




/*
*Looping principal: Lê os comandos, faz o parse e executa.
*/
void funciona(void) {
        printf("//Liguei!\n");
        //Acende a luz vermelha e apaga a verde (AGUARDE)
        IOCLR0 |= 1<<luzes[0];
        IOSET0 |= 1<<luzes[1];

        //Buffer de leitura de linhas de comando
        char palavras[20][30];
        char* ptr[20];
        int i;
        for (i=0; i<20; i++) { 
                ptr[i] = palavras[i];
        }

        //Posiciona a caneta num lugar conhecido
        Reset();
        
        printf("//Esperando ordens...\n");        
        //Lê e executa os comandos
        while (1) {

                //Acende a luz verde e apaga a vermelha (ESPERANDO)
                IOCLR0 |= 1<<luzes[1];
                IOSET0 |= 1<<luzes[0];
                
                //Lê um comando
                int numPalavras = leLinha(ptr);
                
                //Acende a luz vermelha e apaga a verde (OCUPADO)
                IOCLR0 |= 1<<luzes[0];
                IOSET0 |= 1<<luzes[1];

                //Faz ECHO do comando lido
                printf("//");
                for (i=0; i<numPalavras; i++) { 
                        printf("%s", palavras[i]);
                        if (i<numPalavras-1) {
                                printf(" ");
                        } else  {
                                printf("\n");
                        }
                }
                
                //Nenhum comando - ERRO
                if (numPalavras == 0)  {
                        printf("ERROR - No command suplied\n");
                        continue;
                }
                
                //RESET
                if (strcmp (palavras[0], "RESET") == 0) {
                        if (numPalavras == 1) {
                                Reset();
                                printf("OK - System has RESET\n");
                        } else {
                                printf("ERROR - Invalid Number os parameters\n");
                        }
                        
                        continue;
                }

                //UP - Sobe a caneta
                if (strcmp (palavras[0], "UP") == 0) {
                        if (numPalavras == 1) {
                                sobe(50);
                                printf("OK - Pen is UP\n");
                        } else {
                                printf("ERROR - Invalid Number os parameters\n");
                        }
                        
                        continue;
                }

                //DOWN - Desce a caneta
                if (strcmp (palavras[0], "DOWN") == 0) {
                        if (numPalavras == 1) {
                                desce(50);
                                printf("OK - Pen is DOWN\n");
                        } else {
                                printf("ERROR - Invalid Number os parameters\n");
                        }
                        
                        continue;
                }

                //SetDelay - Altera a velocidade do movimento.
                if (strcmp (palavras[0], "SETDELAY") == 0) {
                        if (numPalavras != 2) {
                                printf("ERROR - Invalid Number os parameters\n");
                        } else {
                                int num;
                                if        (sscanf(palavras[1], "%i", &num) != 1) {
                                        printf("ERROR - Value isn't a number\n");
                                } else {
                                        delayconst = num;
                                        printf("OK - Changed the delay to %i\n", num);
                                }
                        }
                        
                        continue;
                }

                //Move - Desloca a caneta para a posição especificada.
                if (strcmp (palavras[0], "MOVE") == 0) {
                        if (numPalavras != 3) {
                                printf("ERROR - Invalid Number os parameters\n");
                        } else {
                                int x, y;
                                if        (sscanf(palavras[1], "%i", &x) != 1) {
                                        printf("ERROR - X isn't a number\n");
                                } else if (sscanf(palavras[2], "%i", &y) != 1) {
                                        printf("ERROR - Y isn't a number\n");
                                } else if ( (x<0) || (y<0) || (x>maxX) || (y>maxY) ) {
                                        printf("ERROR - position is outside coverage area. Max value: [%i, %i]\n", maxX, maxY);
                                } else {
                                        move (x, y);
                                        printf("OK - Moved to [%i, %i]\n", x, y);
                                }
                        }
                        
                        continue;
                }
                
                //SetPin - Liga a saída do pino especificado
                //Usado apenas para depuração! Não deve ser usado normalmente!
                if (strcmp (palavras[0], "SETPIN") == 0) {
                        if (numPalavras != 2) {
                                printf("ERROR - Invalid Number os parameters\n");
                        } else {
                                int pin;
                                if (sscanf(palavras[1], "%i", &pin) != 1) {
                                        printf("ERROR - Value isn't a number\n");
                                } else {
                                        IOSET0 |= 1<<pin;
                                        printf("OK - Pin %i is ON\n", pin);
                                }
                        }
                        continue;
                }
                
                //ClearPin - Desliga a saída do pino especificado
                //Usado apenas para depuração! Não deve ser usado normalmente!
                if (strcmp (palavras[0], "CLEARPIN") == 0) {
                        if (numPalavras != 2) {
                                printf("ERROR - Invalid Number os parameters\n");
                        } else {
                                int pin;
                                if (sscanf(palavras[1], "%i", &pin) != 1) {
                                        printf("ERROR - Value isn't a number\n");
                                } else {
                                        IOCLR0 |= 1<<pin;
                                        printf("OK - Pin %i is OFF\n", pin);
                                }
                        }
                        continue;
                }
                
                //ReadPin - Lê o valor do pino especificado
                //Usado apenas para depuração! Não deve ser usado normalmente!
                 if (strcmp (palavras[0], "READPIN") == 0) {
                        if (numPalavras != 2) {
                                printf("ERROR - Invalid Number os parameters\n");
                        } else {
                                int pin;
                                if (sscanf(palavras[1], "%i", &pin) != 1) {
                                        printf("ERROR - Value isn't a number\n");
                                } else {
                                        int val = (IOPIN0 & (1<<pin))?1:0;
                                        printf("OK - Pin %i = '%i'\n", pin, val);
                                }
                        }
                        continue;
                }
                
                //ReadPin - Lê o valor de todos os pinos
                if (strcmp (palavras[0], "READPINS") == 0) {
                        if (numPalavras != 1) {
                                printf("ERROR - Invalid Number os parameters\n");
                        } else {
                                for (i=0; i<32; i++)  {
                                        printf("//P0.%i = %i\n", i, (IOPIN0 & (1<<i))?1:0);
                                }
                                printf("OK - P0 = %08x\n", (unsigned int)IOPIN0);
                        }
                        continue;
                }
                
                //Default: Comando inválido
                printf("ERRO - Unknown command: '%s'\n", palavras[0]);
                continue;
        }
}
