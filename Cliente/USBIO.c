#define CMD_QUEUE_LEN 0
#define CMD_RESET 1
#define CMD_MOVE 2
#define CMD_LINE 3



#include <stdio.h>
#include <unistd.h>
#include <sys/timeb.h>

#include "usb.h"

typedef unsigned int U32;
typedef unsigned char U8;

// USB device specific definitions
#define VENDOR_ID	0xFFFF
#define PRODUCT_ID	0x0004

#define BULK_IN_EP			0x82
#define BULK_OUT_EP			0x05


static struct usb_device * find_device(int iVendor, int iProduct)
{
	struct usb_bus	*usb_bus;
	struct usb_device *dev;	
	
	for (usb_bus = usb_get_busses(); usb_bus; usb_bus = usb_bus->next) {
		for (dev = usb_bus->devices; dev; dev = dev->next) {
			if ((dev->descriptor.idVendor == iVendor) && 
				(dev->descriptor.idProduct == iProduct)) {
				return dev;
			}
		}
	}
	return NULL;
}

/*
Lê uma linha de comando da porta serial, e quebra ela em várias palavras, retornando o numero de palavras lidas.
*/
static int leLinha(char** palavras) {
        //Indice da palavra sendo lida
        int palavra = 0;
        //Indice do caractere da palavra sendo lida
        int charIndex = 0;               

        //Lê e processa os caracteres.
        while (1) {
                int c;
                //Verifica a porta até ter alguma coisa pra ler.
                do {
                        c = getchar();
                } while (c == -1);
                
                //ESPAÇO ==> pula pra próxima palavra
                if (c <= ' ') {
                        if (charIndex != 0) {
                                //Termina a palavra com '\0'
                                palavras[palavra][charIndex] = 0;
                                //Pula pro começo da próxima palavra
                                palavra++;
                                charIndex = 0;
                        }
                //ENTER ==> retorna
                if ((c == 13) || (c == 10)) {
                        //Termina a palavra atual
                        palavras[palavra][charIndex] = '\0';
                        //Se a palavra atual tava vazia, descarta
                        if (charIndex == 0) 
                                palavra--;
                        //Retorna o numero de palavras lidas.
                        return palavra+1;
                }


                //Letrinhas de verdade
                } else {                        
                        //Passa pra uppercase
                        if (c >='a' && c<='z') {
                                c += 'A' -'a';
                        }
                                
                        //só considera A-Z, 0-9
                        if ( (c >='A' && c<='Z') || 
                             (c >='0' && c<='9') )
                        {
                                //Coloca no fim da palavra atual
                                palavras[palavra][charIndex++] = c;
                        }
                }
        }
}



int main(int argc, char *argv[])
{
	char packet[5];
        char palavras[20][30];
        char* ptr[20];
        int i;
        for (i=0; i<20; i++) { 
                ptr[i] = palavras[i];
        }



	struct usb_device *dev;	
	struct usb_dev_handle *hdl;

	usb_init();
	usb_find_busses();
	usb_find_devices();
	
	dev = find_device(VENDOR_ID, PRODUCT_ID);
	if (dev == NULL) {
		printf("//device not found\n");
		return -1;
	}
	
	hdl = usb_open(dev);
	
	i = usb_set_configuration(hdl, 1);
	if (i < 0) {
		printf("//usb_set_configuration failed: %d\n", i);
	}
	
	i = usb_claim_interface(hdl, 0);
	if (i < 0) {
		printf("//usb_claim_interface failed %d\n", i);
		return -1;
	}

	while (1) {
		fflush(stdout);
		//Lê Quantos comandos podem ser enfileirados
		packet[0] = CMD_QUEUE_LEN;
		i = usb_bulk_write(hdl, 0x05, (char *)packet, 1, 2000);
		if (i < 0) {
			printf("//usb_bulk_write failed %d\n", i);
			usleep(100*1000);
			continue;
		}
		i = usb_bulk_read(hdl, 0x82, (char *)packet, sizeof(packet), 2000);
		if (i < 0) {
			printf("//usb_bulk_read failed %d\n", i);
			usleep(100*1000);
			continue;
		}

		int numComandos=(unsigned char)packet[0];
		int k;
		printf("Posso Mandar %i comandos pro buffer!\n", numComandos);

		if (numComandos == 0) {
			usleep(100*1000);
		}
		
		//Enfileira N comandos
		for (k=0; k<numComandos; k++) {
			fflush(stdout);
			int numPalavras = leLinha(ptr);

			//Nenhum comando - ERRO
			if (numPalavras == 0)  {
				continue;
			}
			//Faz ECHO do comando lido
			printf("OK - ");
			for (i=0; i<numPalavras; i++) { 
				printf("%s", palavras[i]);
				if (i<numPalavras-1) {
					printf(" ");
				} else  {
					printf("\n");
				}
			}

			
			//RESET
			if (strcmp (palavras[0], "RESET") == 0) {
				packet[0] = CMD_RESET;
				i = usb_bulk_write(hdl, 0x05, (char *)packet, 1, 2000);
				if (i < 0) {
					printf("//usb_bulk_write failed %d\n", i);
					return;
				}
			}

			//Move - Desloca a caneta para a posição especificada.
			if (strcmp (palavras[0], "MOVE") == 0) {
				int x, y;
				sscanf(palavras[1], "%i", &x);
				sscanf(palavras[2], "%i", &y);

				packet[0] = CMD_MOVE;
				packet[1] = (x>>8) & 0xFF;
				packet[2] = (x>>0) & 0xFF;
				packet[3] = (y>>8) & 0xFF;
				packet[4] = (y>>0) & 0xFF;
				i = usb_bulk_write(hdl, 0x05, (char *)packet, 5, 2000);
				if (i < 0) {
					printf("//usb_bulk_write failed %d\n", i);
					return;
				}
			}

			//Move - Desloca a caneta para a posição especificada.
			if (strcmp (palavras[0], "LINE") == 0) {
				int x1, y1, x2, y2;
				sscanf(palavras[1], "%i", &x1);
				sscanf(palavras[2], "%i", &y1);
				sscanf(palavras[3], "%i", &x2);
				sscanf(palavras[4], "%i", &y2);

				packet[0] = CMD_LINE;
				packet[1] = (x1>>8) & 0xFF;
				packet[2] = (x1>>0) & 0xFF;
				packet[3] = (y1>>8) & 0xFF;
				packet[4] = (y1>>0) & 0xFF;
				packet[5] = (x2>>8) & 0xFF;
				packet[6] = (x2>>0) & 0xFF;
				packet[7] = (y2>>8) & 0xFF;
				packet[8] = (y2>>0) & 0xFF;

				i = usb_bulk_write(hdl, 0x05, (char *)packet, 9, 2000);
				if (i < 0) {
					printf("//usb_bulk_write failed %d\n", i);
					return;
				}
			}

			if (strcmp (palavras[0], "BYE") == 0) {
				usb_release_interface(hdl, 0);
				usb_close(hdl);
				return;
			}
		}
	}

	return 0;
}

