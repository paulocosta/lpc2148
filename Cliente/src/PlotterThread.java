import java.awt.Color;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;


public class PlotterThread extends Thread {
	FormMain form;
	Queue<Linha> linhas = new LinkedList<Linha>();
	Process procComm;
	Point curPosition = new Point(-1,-1);
	Semaphore sem = new Semaphore(0);
	boolean ret = true; //true se o último comando deu OK
	boolean needsReset = false;
	
	BufferedReader responseStream;
	PrintStream commandStream;
	
	public PlotterThread(FormMain mainForm) throws IOException {
		this.form = mainForm;
		this.procComm = Runtime.getRuntime().exec("sudo /Paulo/ARM/Cliente/USBIO");
		responseStream = new BufferedReader(new InputStreamReader(procComm.getInputStream()));
		commandStream = new PrintStream(procComm.getOutputStream());
	}
	
	Point corrigeCoordenadas(Point p) {
		return new Point(
				FormMain.maxX - p.x * FormMain.maxX / (form.image.getWidth()-1),
				p.y * FormMain.maxY / (form.image.getHeight()-1) );
	}
	
	synchronized void reset() {
		needsReset = true;
	}
	
	void poeLinha (Linha l) {
		synchronized(linhas) {
			linhas.add(l);
		}

		synchronized (form.graphics) {
			form.graphics.setColor(Color.GRAY);
			form.graphics.drawLine(
					(int)l.A.getX(),
					(int)l.A.getY(),
					(int)l.B.getX(),
					(int)l.B.getY());
		}
		form.repaint();
	}
	
	boolean executa(String cmd) {
		synchronized (commandStream) {
			commandStream.println(cmd);
			//System.err.println(command);
			commandStream.flush();
			/*try {
				sem.acquire();
			} catch (InterruptedException e1) {
				return false;
			}*/
			return true;
		}
	}
	
	boolean move(Point p){
		p = corrigeCoordenadas(p);
		return executa("move " + (int)p.getX() + " " + (int)p.getY());
	}
	boolean line(Point a, Point b){
		a = corrigeCoordenadas(a);
		b = corrigeCoordenadas(b);
		return executa("line " + (int)a.getX() + " " + (int)a.getY() + " " + (int)b.getX() + " " + (int)b.getY());
	}
	
	@Override
	public void run() {
		Thread responseThread = new Thread() {
			@Override
			public void run() {
				while (true) {
					String line = null;
					try {					
						line = responseStream.readLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (line == null) 
						break;
					System.out.println(line);
					if (line.startsWith("//"))
						continue; //Linha Ignorável
					
					if (line.startsWith("OK")) {
						ret = true;
					} else {
						ret = false;
					}
					sem.release();
				}
			}
		};
		responseThread.start();
		
		while (form.isVisible()) {
			if (needsReset) {
				move(new Point(0,0));
				needsReset = false;
			}
			
			Linha proximaLinha;
			synchronized(linhas) {
				proximaLinha = linhas.poll();
			}
			if (proximaLinha == null) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {}
				continue;
			}
			
			line(proximaLinha.A, proximaLinha.B);
			
			synchronized (form.graphics) {
				form.graphics.setColor(Color.BLUE);
				form.graphics.drawLine(
						(int)proximaLinha.A.getX(),
						(int)proximaLinha.A.getY(),
						(int)proximaLinha.B.getX(),
						(int)proximaLinha.B.getY());
			}
			form.repaint();/*
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {}*/
		}
		executa("BYE");
		commandStream.close();
		
	}
}
