import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class FormMain extends JFrame implements MouseListener, MouseMotionListener{
	public static int maxX = 2750;
	public static int maxY = 930;
	public static int tamX = 224;
	public static int tamY = 197;
	
	/*JMenuItem itemImprimirTempoReal = new JRadioButtonMenuItem("Imprimir em tempo real");
	JMenuItem itemImprimirTudo      = new JMenuItem("Imprimir tudo");
	JMenuItem itemNaoImprimir       = new JMenuItem("Plotter parado");
	*/
	
	BufferedImage image = new BufferedImage(4*tamX, 4*tamY, BufferedImage.TYPE_INT_RGB);
	Graphics2D graphics = image.createGraphics();
	PlotterThread plotterThread = new PlotterThread(this);
	Point posMouseDown = new Point();
	
	public FormMain() throws IOException {
		super("Programa controlador do Plotter");
		JPanel panel = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				g.drawImage(image, 0, 0, null);
			}
		};
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		panel.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		panel.addMouseMotionListener(this);
		panel.addMouseListener(this);
		add(panel);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		
		
		
		JMenuBar menuBar = new JMenuBar();
		JMenu menuOptions = new JMenu("Opções");
		JMenuItem itemLimparTela = new JMenuItem("Limpar a tela");
		
		JMenuItem itemReset = new JMenuItem("Mover para o cantinho");
		//JMenuItem itemImprimirTudo      = new JMenuItem("Imprimir tudo");
		//MenuItem itemNaoImprimir       = new JMenuItem("Plotter parado");
		this.add(menuBar, BorderLayout.NORTH);
		menuBar.add(menuOptions);
		menuOptions.add(itemLimparTela);
		menuOptions.add(itemReset);
		//menuOptions.add(itemImprimirTudo);
		//menuOptions.add(itemNaoImprimir);
		
		ActionListener actionLimpar = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				synchronized (graphics) {
					graphics.setColor(Color.WHITE);
					graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
					repaint();
				}
			}
		};
		itemLimparTela.addActionListener(actionLimpar);
		actionLimpar.actionPerformed(null);

		
		
		ActionListener actionReset = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				plotterThread.reset();
			}
		};
		itemReset.addActionListener(actionReset);
		actionLimpar.actionPerformed(null);
		
		pack();
		setVisible(true);
		plotterThread.start();
	}
	
	public static void main(String[] args) throws IOException {
		new FormMain();
	}	
	
	
	@Override
	public void mouseDragged(MouseEvent e) {
		Point dest = e.getPoint();
		plotterThread.poeLinha(new Linha (posMouseDown, dest));
		posMouseDown = dest;
	}
	public void mousePressed(MouseEvent e) {
		posMouseDown = e.getPoint();
	}

	public void mouseReleased(MouseEvent e) {}

	public void mouseMoved(MouseEvent e) {}

	public void mouseClicked(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}
}
